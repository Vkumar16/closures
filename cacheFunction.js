function cacheFunction(cb) {
    // Should return a function that invokes `cb`.                                          done
    // A cache (object) should be kept in closure scope.                                    done
    // The cache should keep track of all arguments have been used to invoke this function. done
    // If the returned function is invoked with arguments that it has already seen          done
    // then it should return the cached result and not invoke `cb` again.                   done
    // `cb` should only ever be invoked once for a given set of arguments.                  done
    let cache = {};
    function invokeCb(key, value) {
        if (!cache[key])
            cache = cb(cache, key, value);
        return cache;
    }
    return invokeCb;
}

module.exports = cacheFunction;