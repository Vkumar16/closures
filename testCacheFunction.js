const cacheFunction = require("./cacheFunction");

const fun = (cache, key, value) => {
    cache[key] = value;
    return cache;
}
const invokeCb = cacheFunction(fun);

invokeCb("name", "vishal kumar");
invokeCb("age", 23);
invokeCb("aggress", "Ara Bihar");

const cache = invokeCb("name", "manoje kumar");
console.log(cache);
