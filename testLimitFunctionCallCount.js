const limitFunctionCallCount = require('./limitFunctionCallCount');

const fun = (count) => count+1;

const invokeFunction = limitFunctionCallCount(fun, 4);
invokeFunction();
