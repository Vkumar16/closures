function limitFunctionCallCount(cb, n) {
    // Should return a function that invokes `cb`.    done 
    // The returned function should only allow `cb` to be invoked `n` times. done
    // Returning null is acceptable if cb can't be returned

    let count = 0;
    return function () {
        for (let i = 0; i < n; i++) {
            count = cb(count);
        }
        console.log(count);
    }

}
module.exports = limitFunctionCallCount;