const counterFactory = require('./counterFactory');

const counterObject = counterFactory();

counterObject.increment();
counterObject.increment();
counterObject.increment();

let incrementValue = counterObject.increment();
console.log(incrementValue);

counterObject.decrement();
let decrementObject = counterObject.decrement();
console.log(decrementObject);
