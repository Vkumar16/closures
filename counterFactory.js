function counterFactory() {
    let counter = 0;
    function changeCounterValue(value) {
        counter += value;
        return counter;
    }
    return {
        increment: function () {
            return changeCounterValue(1);
        },
        decrement: function () {
            return changeCounterValue(-1);
        }
    }
}
module.exports = counterFactory;